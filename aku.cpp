///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/22/21
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm{
   Aku::Aku(float weight,enum Color newColor,enum Gender newGender){
      scaleColor = newColor;
      favTemp = 75;
      gender = newGender;
      lbs = weight;
      species = "Katsuwonus pelamis";
   } 

   void Aku::printInfo(){
      cout << "Aku" << endl;
      cout << "   Weight = [" << lbs << "]" << endl;
      Fish::printInfo();
   }

   const string Fish::speak(){
      return string("Bubble bubble");
   }
   
}
