///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   3/25/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <iostream>
#include <cstdlib>

#include "animal.hpp"
#include "dog.hpp"
#include "cat.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
namespace animalfarm{ 
   
   class AnimalFactory{
      public:
         static Animal* getRandomAnimal();
   };
}//namespace animalfarm
