///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   @Todo
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "animal.hpp"
#include "list.hpp"
#include "animalfactory.hpp"
#include "node.hpp"
using namespace std;
using namespace animalfarm;
int Animal::i;
int Animal::length;
char Animal::randomName[10];
int main(){
   SingleLinkedList list;
   cout << "TESTING" << endl;
   list.push_front((Node*)AnimalFactory::getRandomAnimal()); 
   cout << "Is it Empty " << list.get_first() << endl;
}
