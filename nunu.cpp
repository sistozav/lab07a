///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "nunu.hpp"

using namespace std;

namespace animalfarm{
   Nunu::Nunu(bool isNative,enum Color newColor,enum Gender newGender){
      scaleColor = newColor;
      gender = newGender;
      favTemp = 80.6;
      species = "Fistularia chinensis";
      nav = isNative;
   }

   void Nunu::printInfo(){
      cout <<"Nunu" << endl;
      cout <<"   Is native = ["<< native (nav) <<"]" << endl;
      Fish::printInfo();
   }
   const string speak(){
      return string("Bubble bubble");
   }
   string Nunu::native (bool nav){
      switch(nav){
         case true : return string("true"); break;
         case false: return string("false"); break;
      }
   }
}




