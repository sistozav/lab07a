///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   @Todo
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <ctime>
#include "animal.hpp"
#include "animalfactory.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;
int Animal::i;
int Animal::length;
char Animal::randomName[10];
int main(){
   cout << "Welcome to Animal Farm 4" << endl;
   /////////////////////////////// ///////////////////////////////
   /////////////////////////////// Animal List ///////////////////////////////
   /////////////////////////////// ///////////////////////////////
   SingleLinkedList animalList; // Instantiate a SingleLinkedList
   for( auto i = 0 ; i < 25 ; i++ ) {
   animalList.push_front((Node*)AnimalFactory::getRandomAnimal() ) ;
   }
   cout << endl;
   cout << "List of Animals" << endl;
   cout << " Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << " Number of elements: " << animalList.size() << endl;
   for( auto animal = animalList.get_first() // for() initialize
      ; animal != nullptr // for() test
      ; animal = animalList.get_next( animal )) { // for() increment
      cout << ((Animal*)animal)->speak() << endl;
      }
   while( !animalList.empty() ) {
   Animal* animal = (Animal*) animalList.pop_front();
   delete animal;
   }
}
