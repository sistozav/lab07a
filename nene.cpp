///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/25/21
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "nene.hpp"

using namespace std;

namespace animalfarm{
   Nene::Nene(string tagID,enum Color newColor,enum Gender newGender){
      id = tagID;
      featherColor = newColor;
      gender = newGender;
      isMigratory = true;
      species = "Branta sandvicensis";
   }

   const string Nene::speak(){
      return string("Nay, nay");
   }

   void Nene::printInfo(){
      cout << "Nene" << endl;
      cout << "   Tag ID = [" << id << "]" << endl;
      Bird::printInfo();
   }
}
