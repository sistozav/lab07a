///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/22/21
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "fish.hpp"

using namespace std;

namespace animalfarm{
   class Aku : public Fish{
      public:
         float lbs;
         Aku(float weight,enum Color newColor, enum Gender newGender);
         
         void printInfo();
   };
}
