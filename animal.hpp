///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include "node.hpp"

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color {BLACK,WHITE,RED,BLUE,GREEN,PINK,SILVER,YELLOW,BROWN};

class Animal:public Node{
public:
	static int i;
   static int length;
   static char randomName[10];
   enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
	void printInfo();

   Animal();
   ~Animal();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const bool getRandomBool();
   static const float getRandomWeight();
   static const string getRandomName();
};

} // namespace animalfarm
