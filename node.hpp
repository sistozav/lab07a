///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   @Todo
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace animalfarm{
   class Node{
      protected:
         Node* next = nullptr;
         friend class SingleLinkedList;
   };
}//namespace animalfarm

