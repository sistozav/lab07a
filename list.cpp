///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   3/28/21
///////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include "list.hpp"
using namespace std;
namespace animalfarm{
   
   const bool SingleLinkedList::empty() const{
      Node* tempNode = head;
      if(tempNode == nullptr)
         return true;
      else
         return false;
  };
  void SingleLinkedList::push_front(Node* newNode){
     newNode->next = head;
     head = newNode;
  };
  Node* SingleLinkedList::pop_front(){
      Node* tempNode = head;
      if(tempNode == nullptr)
         return nullptr;
      else
      {
         head = tempNode->next;
         return tempNode;
      }

  };
  Node* SingleLinkedList::get_first() const{
      Node* tempNode = head;
      return tempNode;
  };
  Node* SingleLinkedList::get_next(const Node* currentNode) const{
     Node* tempNode; 
     tempNode = currentNode->next;
     return tempNode;
  };
  unsigned int SingleLinkedList::size() const{
      unsigned int i=0;
      Node* tempNode = head;
      while(tempNode != nullptr){
         tempNode = tempNode->next;
         i++;
      }
      return i;
  };
}
