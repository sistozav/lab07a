///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}
Animal::Animal(){
   cout <<".";
}
Animal::~Animal(){
   cout << "x";
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK: return string("Black"); break;
      case WHITE: return string("White"); break;
      case RED:   return string("Red"); break;
      case BLUE:  return string("Blue"); break;
      case GREEN: return string("Green"); break;
      case PINK:  return string("Pink"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN: return string("Brown"); break;
   }
   return string("Unknown");
};
const Gender Animal::getRandomGender(){
   int i;
   i = (rand()%3);
   switch(i){
      case 0: return MALE; break;
      case 1: return FEMALE; break;
      default: return UNKNOWN; break;
   }
};

const Color Animal::getRandomColor(){
      i = (rand()%9);
      switch(i){
         case 0:  return BLACK;  break;
         case 1:  return WHITE;  break;
         case 2:  return RED;    break;
         case 3:  return BLUE;   break;
         case 4:  return GREEN;  break;
         case 5:  return PINK;   break;
         case 6:  return SILVER; break;
         case 7:  return YELLOW; break;
         default: return BROWN; break;
      }
};
const string Animal::getRandomName(){
      length = (rand()%6)+4;
      randomName[0] = (rand()%26)+65;
      for(i=1 ; i<=length ; i++)
         randomName[i] = (rand()%26)+97;
      return randomName;
};
const bool Animal::getRandomBool(){
   int i;
   i = (rand()%2);
   switch(i){
      case 0: return false; break;
      default: return true;  break;
   }
};
const float Animal::getRandomWeight(){
      i = (rand()%75)+1;
      return i;
};
	
} // namespace animalfarm
