///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   3/28/21
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"

namespace animalfarm{
   class SingleLinkedList{
      protected:
         Node* head = nullptr;
      public:
         const bool empty() const;
         void  push_front(Node* newNodel);
         Node* pop_front();
         Node* get_first() const;
         Node* get_next(const Node* currentNode) const;
         unsigned int size() const;
   };
}//namespace animalfarm
