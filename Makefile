###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Vinton Sistoza  <sistozav@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   2/24/21
###############################################################################

all: main

main.o: animal.hpp main.cpp
	g++ -c main.cpp

main2.o: animal.hpp main2.cpp animalfactory.hpp list.hpp node.hpp
	g++ -c main2.cpp

testfunc.o: testfunc.cpp list.hpp
	g++ -c testfunc.cpp

animal.o: animal.hpp animal.cpp node.hpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: animal.hpp fish.cpp fish.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.hpp palila.cpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.hpp nene.cpp bird.hpp
	g++ -c nene.cpp


animalfactory.o: animalfactory.cpp animalfactory.hpp animal.hpp
	g++ -c animalfactory.cpp

node.o: node.hpp
	g++ -c node.cpp

list.o: list.hpp node.hpp
	g++ -c list.cpp 

testfunc: testfunc.cpp *.hpp testfunc.o list.o node.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalfactory.o
	g++ -o testfunc testfunc.o animal.o animalfactory.o list.o node.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o

oldmain: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o

main: main2.cpp *.hpp main2.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalfactory.o node.o list.o
	g++ -o main main2.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalfactory.o node.o list.o
	
clean:
	rm -f *.o main
