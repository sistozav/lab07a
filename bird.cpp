///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/22/21
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "bird.hpp"

using namespace std;

namespace animalfarm{
   void Bird::printInfo(){
      Animal::printInfo();
      cout << "   Feather Color = [" << colorName(featherColor) << "]" << endl;
      cout << "   Is Migratory = ["<< native(isMigratory) << "]" << endl;
   }
   string Bird::native (bool isMigratory){
      switch(isMigratory){
         case true : return string("true"); break;
         case false: return string("false"); break;
      }
   }

}
