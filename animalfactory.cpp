///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   3/25/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

#include "animalfactory.hpp"

using namespace std;
namespace animalfarm{

    Animal* AnimalFactory::getRandomAnimal(){
      Animal* newAnimal = NULL;
      int i = (rand()%6);
      switch(i){
         case 0: newAnimal = new Cat    (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 1: newAnimal = new Dog    (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 2: newAnimal = new Nunu   (Animal::getRandomBool(), RED, Animal::getRandomGender());                      break;
         case 3: newAnimal = new Aku    (Animal::getRandomWeight(), SILVER, Animal::getRandomGender());                 break;
         case 4: newAnimal = new Palila (Animal::getRandomName(), YELLOW, Animal::getRandomGender());                   break;
         case 5: newAnimal = new Nene   (Animal::getRandomName(), BROWN, Animal::getRandomGender());                    break;
         default: newAnimal = NULL; break;
         }
      return newAnimal;
   };
} //animalfarm
